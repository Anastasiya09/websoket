module Notifier
  def self.publish_notification(message)
    Thread.new {
      EM.run {
        faye = Rails.env.development? ? 'http://127.0.0.1:3000/faye' :  ''
        client = Faye::Client.new(faye)
        publisher = client.publish('/get_bonus', { message: message })

        publisher.callback do
          Rails.logger.debug 'Message received by client!'
        end

        publisher.errback do |error|
          Rails.logger.debug "There was a problem: #{error.message}"
        end
      }
    }
  end
end