Rails.application.routes.draw do

  resources :home, only: [:index] do
    collection do
      get :get_bonus
    end
  end

  root 'home#index'
end
