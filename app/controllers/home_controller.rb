class HomeController < ApplicationController
  def index
  end

  def get_bonus
    Notifier.publish_notification(params[:message])
    render nothing: true, status: 200
  end
end
